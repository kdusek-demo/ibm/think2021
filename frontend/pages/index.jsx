import Head from 'next/head'
import {api_config} from "../config";
import ReactGA from 'react-ga';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Homepage from "../components/homepage";
import React from "react";

export default function Home(props) {
  return (
    <Router>
      <Head>
        <title>MagiCALC</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <Switch>
          <Route path={"/"} exact component={Homepage} />
        </Switch>
    </Router>
  )
}