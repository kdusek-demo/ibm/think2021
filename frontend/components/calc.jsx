import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button";
import {useState} from "react";
import {api_config} from "../config";

export default function Calc(props){
    const [value1, setValue1] = useState(null);
    const [value2, setValue2] = useState(null);
    const [result, setResult] = useState(null);
    const [error, setError] = useState(null);
    const [func, setFunc] = useState('addition');
    const runCalc = async function(e){
        e.preventDefault();
        setResult(null);
        setError(null);
        try {
            if (!func || !value1 || !value2){
                setError("Enter all values");
                return;
            }
            let response = await fetch(api_config + "/calc/" + func + "/" + value1 + "/" + value2, {
                headers: { 'Content-Type': 'application/json' },
                method: "GET"
            });
            let result = await response.json();
            if (result.result) {
                setResult(result.result);
            } else if (result.error) {
                setResult(result.error);
            }
        } catch (e) {
            setError(e.message);
        }
    }

    const _handleFormChange = async function (e){
        if (e.currentTarget.id === 'value1'){
            setValue1(e.currentTarget.value);
        }
        if (e.currentTarget.id == 'value2'){
            setValue2(e.currentTarget.value);
        }
    }

    const _setFunction = function(e){
        setFunc(e.currentTarget.id);
    }

    return <>
        <Form onSubmit={runCalc}>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="calcRadios" id="addition"
                        checked onChange={_setFunction} />
                    <label className="form-check-label" htmlFor="exampleRadios1">
                        Addition
                    </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="calcRadios" id="subtraction"
                       onChange={_setFunction} />
                    <label className="form-check-label" htmlFor="exampleRadios2">
                        Subtraction
                    </label>
            </div>
            <div className="form-check disabled">
                <input className="form-check-input" type="radio" name="calcRadios" id="multiplication"
                       value="multiplication" disabled onChange={_setFunction}  />
                    <label className="form-check-label" htmlFor="exampleRadios3">
                        Multiplication
                    </label>
            </div>

            <div className={"mt-3"}>
                <Form.Group controlId="value1">
                    <Form.Label>Value 1</Form.Label>
                    <Form.Control onChange={_handleFormChange} type="text" className="dark-cyan-border"/>
                </Form.Group>
                <Form.Group controlId="value2">
                    <Form.Label>Value 2</Form.Label>
                    <Form.Control onChange={_handleFormChange} type="text" className="dark-cyan-border"/>
                </Form.Group>
            </div>
            <div>
                <Form.Group className="d-flex justify-content-center">
                    <Button variant="primary" className="rounded-button w-50" type="submit">
                        CALC
                    </Button>
                </Form.Group>
            </div>
            <div className={"mb-3"}>
                {result ? <><div className='text-success'>Result</div>{result} </> : ""}
                {error ? <><div className="text-danger">Error</div>{error}</>  : ""}
            </div>
        </Form>
    </>
}