const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000;
const bodyParser = require('body-parser');
const calc = require('./lib/calc.js');
// app.use(cors({
//   origin: "*"
// }))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsi
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.use('/calc', calc.router);

app.listen(port, () => {
  console.log(`API listening at http://localhost:${port}`)
})
